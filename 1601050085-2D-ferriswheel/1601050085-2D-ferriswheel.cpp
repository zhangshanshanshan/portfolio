#include "stdafx.h"
#include <glut.h>
#include <math.h>

#define PI 3.14159  //设置圆周率
#define N 1000

int n = 3, R = 10;  //多边形变数，外接圆半径
float theta = 180.0;  //旋转初始角度值
int as = 0;//自转的旋转角
int ase = 0;//公转角
int line[N][4], k = 0;
int ww, hh;
int f = 30;

void Display(void);
void Reshape(GLsizei w, GLsizei h);
void mytime(int value);
void myinit(void);
void myMouse(int button, int state, int x, int y);
void myMotion(int x, int y);
void draw();//五角星

void dizuo(void);//摩天轮的底座
void dayuan(void);//摩天轮最大的旋转圆
void zuoweicang(void);//摩天轮的座位仓

void selectFont(int size, int charset, const char* face); //选择字体
void drawCNString(const char* str); //生成中文字体函数


int APIENTRY _tWinMain(HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPTSTR    lpCmdLine,
	int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);
	char *argv[] = { "hello ", " " };
	int argc = 2; // must/should match the number of strings in argv
	glutInit(&argc, argv);  //初始化GLUT库；
	glutInitWindowSize(700, 700);  //设置显示窗口大小
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);  //设置显示模式；（注意双缓冲）
	glutCreateWindow("摩天轮"); // 创建显示窗口
	glutDisplayFunc(Display);  //注册显示回调函数
	glutReshapeFunc(Reshape);  //注册窗口改变回调函数
	glutMouseFunc(myMouse);//注册鼠标响应函数
	myinit();
	glutTimerFunc(200, mytime, 10);
	glutMainLoop();  //进入事件处理循环
	return 0;
}

void myinit()
{
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);//反走样
	glEnable(GL_BLEND);
	glEnable(GL_POINT_SMOOTH);
	glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);
	glEnable(GL_LINE_SMOOTH);
	glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
	glEnable(GL_POLYGON_SMOOTH);
	glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
	glLineWidth(3.0);
}

void Display(void)
{
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW); //设置矩阵模式为模型变换模式，表示世界坐标系下
	glLoadIdentity();   //将当前矩阵设置为单位矩阵

	dizuo();
	dayuan();
	zuoweicang();

	draw(); 

	glutSwapBuffers();   //双缓冲的刷新模式；
}

void dizuo(void) {

	glPushMatrix();
	glTranslatef(350, 350, 0);//平移函数
	glColor3f((167.0 / 255.0), (172.0 / 255.0), (176.0 / 255.0));
	glRotatef(ase, 0, 0, 1);
	glTranslatef(0, 0, 0);
	glutWireSphere(30, 30, 30);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(350, 350, 0);//平移函数
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); //线框模式
	glColor3f((167.0 / 255.0), (172.0 / 255.0), (176.0 / 255.0));
	glBegin(GL_TRIANGLES);
	glVertex2f(0, 0);
	glVertex2f(-150, -220);
	glVertex2f(150, -220);
	glEnd();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(330, 40, 0);//平移函数
	glColor3f((118.0 / 255.0), (188.0 / 255.0), (211.0 / 255.0));
	glBegin(GL_LINES);//AC
	glVertex2f(0, f);
	glVertex2f(f * cos(54 * PI / 180), -f * sin(54 * PI / 180));//C
	glEnd();
	glBegin(GL_LINES);//AD
	glVertex2f(0, f);
	glVertex2f(-f * cos(54 * PI / 180), -f * sin(54 * PI / 180));//D
	glEnd();
	glBegin(GL_LINES);//EB
	glVertex2f(-f * cos(18 * PI / 180), f * sin(18 * PI / 180));//E
	glVertex2f(f * cos(18 * PI / 180), f * sin(18 * PI / 180));//B
	glEnd();
	glBegin(GL_LINES);//DB
	glVertex2f(-f * cos(54 * PI / 180), -f * sin(54 * PI / 180));//D
	glVertex2f(f * cos(18 * PI / 180), f * sin(18 * PI / 180));//B
	glEnd();
	glBegin(GL_LINES);//EC
	glVertex2f(-f * cos(18 * PI / 180), f * sin(18 * PI / 180));//E
	glVertex2f(f * cos(54 * PI / 180), -f * sin(54 * PI / 180));//C
	glEnd();
	glPopMatrix();

	selectFont(36, GB2312_CHARSET, "楷体_GB2312"); //设置字体楷体48号字
	glRasterPos2f(10, 10);  
	drawCNString("1601050085-张颖姗");
}

void dayuan(void) {
	glPushMatrix();
	glTranslatef(350, 350, 0);//平移函数
	glColor3f((215.0 / 255.0), (211.0 / 255.0), (212.0 / 255.0));
	glRotatef(as, 0, 0, 1);//绕Z轴旋转
	glutWireSphere(150, 30, 30);
	glPopMatrix();
}


void zuoweicang(void) {

	glPushMatrix();//座位仓
	glTranslatef(350, 350, 0);//平移函数
	glPolygonMode(GL_FRONT_AND_BACK,GL_FILL); //实心模式
	glColor3f((169.0 / 255.0), (215.0 / 255.0), (228.0 / 255.0));
	glRotatef(ase, 0, 0, 1);//公转，中心点在摩天轮的原点，即坐标原点
	glTranslatef(150, 0, 0);
	glutWireSphere(10, 30, 30);
	glPopMatrix();

	glPushMatrix();//线
	glTranslatef(350, 350, 0);//平移函数
	glColor3f((169.0 / 255.0), (215.0 / 255.0), (228.0 / 255.0));
	glRotatef(ase, 0, 0, 1);
	glBegin(GL_LINES);
	glVertex2f(0, 0);
	glVertex2f(150,0);
	glEnd();
	glPopMatrix();

	glPushMatrix();//座位仓
	glTranslatef(350, 350, 0);//平移函数
	glColor3f((169.0 / 255.0), (215.0 / 255.0), (228.0 / 255.0));
	glRotatef(ase, 0, 0, 1);//公转，中心点在摩天轮的原点，即坐标原点
	glTranslatef(-150, 0, 0);
	glutWireSphere(10, 30, 30);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(350, 350, 0);//平移函数
	glColor3f((169.0 / 255.0), (215.0 / 255.0), (228.0 / 255.0));
	glRotatef(ase, 0, 0, 1);
	glBegin(GL_LINES);
	glVertex2f(0, 0);
	glVertex2f(-150,0);
	glEnd();
	glPopMatrix();

	glPushMatrix();//座位仓
	glTranslatef(350, 350, 0);//平移函数
	glColor3f((169.0 / 255.0), (215.0 / 255.0), (228.0 / 255.0));
	glRotatef(ase, 0, 0, 1);//公转，中心点在摩天轮的原点，即坐标原点
	glTranslatef(0, -150, 0);
	glutWireSphere(10, 30, 30);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(350, 350, 0);//平移函数
	glColor3f((169.0 / 255.0), (215.0 / 255.0), (228.0 / 255.0));
	glRotatef(ase, 0, 0, 1);
	glBegin(GL_LINES);
	glVertex2f(0, 0);
	glVertex2f(0,-150);
	glEnd();
	glPopMatrix();

	glPushMatrix();//座位仓
	glTranslatef(350, 350, 0);//平移函数
	glColor3f((169.0 / 255.0), (215.0 / 255.0), (228.0 / 255.0));
	glRotatef(ase, 0, 0, 1);//公转，中心点在摩天轮的原点，即坐标原点
	glTranslatef(0, 150, 0);
	glutWireSphere(10, 30, 30);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(350, 350, 0);//平移函数
	glColor3f((169.0 / 255.0), (215.0 / 255.0), (228.0 / 255.0));
	glRotatef(ase, 0, 0, 1);
	glBegin(GL_LINES);
	glVertex2f(0, 0);
	glVertex2f(0,150);
	glEnd();
	glPopMatrix();
	
	glPushMatrix();
	glTranslatef(350, 350, 0);//平移函数
	glColor3f((24.0 / 255.0), (71.0 / 255.0), (86.0 / 255.0));
	glRotatef(ase, 0, 0, 1);
	glTranslatef(129.9, 75, 0);
	glutWireSphere(10, 30, 30);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(350, 350, 0);//平移函数
	glColor3f((24.0 / 255.0), (71.0 / 255.0), (86.0 / 255.0));
	glRotatef(ase, 0, 0, 1);
	glBegin(GL_LINES);
	glVertex2f(0, 0);
	glVertex2f(129.9,75);
	glEnd();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(350, 350, 0);//平移函数
	glColor3f((24.0 / 255.0), (71.0 / 255.0), (86.0 / 255.0));
	glRotatef(ase, 0, 0, 1);
	glTranslatef(-129.9, 75, 0);
	glutWireSphere(10, 30, 30);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(350, 350, 0);//平移函数
	glColor3f((24.0 / 255.0), (71.0 / 255.0), (86.0 / 255.0));
	glRotatef(ase, 0, 0, 1);
	glBegin(GL_LINES);
	glVertex2f(0, 0);
	glVertex2f(-129.9,75);
	glEnd();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(350, 350, 0);//平移函数
	glColor3f((24.0 / 255.0), (71.0 / 255.0), (86.0 / 255.0));
	glRotatef(ase, 0, 0, 1);
	glTranslatef(129.9, -75, 0);
	glutWireSphere(10, 30, 30);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(350, 350, 0);//平移函数
	glColor3f((24.0 / 255.0), (71.0 / 255.0), (86.0 / 255.0));
	glRotatef(ase, 0, 0, 1);
	glBegin(GL_LINES);
	glVertex2f(0, 0);
	glVertex2f(129.9,-75);
	glEnd();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(350, 350, 0);//平移函数
	glColor3f((24.0 / 255.0), (71.0 / 255.0), (86.0 / 255.0));
	glRotatef(ase, 0, 0, 1);
	glTranslatef(-129.9, -75, 0);
	glutWireSphere(10, 30, 30);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(350, 350, 0);//平移函数
	glColor3f((24.0 / 255.0), (71.0 / 255.0), (86.0 / 255.0));
	glRotatef(ase, 0, 0, 1);
	glBegin(GL_LINES);
	glVertex2f(0, 0);
	glVertex2f(-129.9,-75);
	glEnd();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(350, 350, 0);//平移函数
	glColor3f((245.0 / 255.0), (193.0 / 255.0), (205.0 / 255.0));
	glRotatef(ase, 0, 0, 1);
	glTranslatef(75, 129.9, 0);
	glutWireSphere(10, 30, 30);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(350, 350, 0);//平移函数
	glColor3f((245.0 / 255.0), (193.0 / 255.0), (205.0 / 255.0));
	glRotatef(ase, 0, 0, 1);
	glBegin(GL_LINES);
	glVertex2f(0, 0);
	glVertex2f(75, 129.9);
	glEnd();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(350, 350, 0);//平移函数
	glColor3f((245.0 / 255.0), (193.0 / 255.0), (205.0 / 255.0));
	glRotatef(ase, 0, 0, 1);
	glTranslatef(-75, 129.9, 0);
	glutWireSphere(10, 30, 30);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(350, 350, 0);//平移函数
	glColor3f((245.0 / 255.0), (193.0 / 255.0), (205.0 / 255.0));
	glRotatef(ase, 0, 0, 1);
	glBegin(GL_LINES);
	glVertex2f(0, 0);
	glVertex2f(-75, 129.9);
	glEnd();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(350, 350, 0);//平移函数
	glColor3f((245.0 / 255.0), (193.0 / 255.0), (205.0 / 255.0));
	glRotatef(ase, 0, 0, 1);
	glTranslatef(75, -129.9, 0);
	glutWireSphere(10, 30, 30);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(350, 350, 0);//平移函数
	glColor3f((245.0 / 255.0), (193.0 / 255.0), (205.0 / 255.0));
	glRotatef(ase, 0, 0, 1);
	glBegin(GL_LINES);
	glVertex2f(0, 0);
	glVertex2f(75, -129.9);
	glEnd();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(350, 350, 0);//平移函数
	glColor3f((245.0 / 255.0), (193.0 / 255.0), (205.0 / 255.0));
	glRotatef(ase, 0, 0, 1);
	glTranslatef(-75, -129.9, 0);
	glutWireSphere(10, 30, 30);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(350, 350, 0);//平移函数
	glColor3f((245.0 / 255.0), (193.0 / 255.0), (205.0 / 255.0));
	glRotatef(ase, 0, 0, 1);
	glBegin(GL_LINES);
	glVertex2f(0, 0);
	glVertex2f(-75, -129.9);
	glEnd();
	glPopMatrix();
}


void mytime(int value)
{
	as += 1;
	ase += 1;//公转的转速要同步

	if ((f > 0) && (f <= 30)) {
		f -= 3;
	}
	else {
		f = 20;
	}
	
	
	glutPostRedisplay();  //重画，相当于重新调用Display();

	glutTimerFunc(200, mytime, 10);//每隔200毫秒，调用一次函数

}
void Reshape(GLsizei w, GLsizei h)
{
	glMatrixMode(GL_PROJECTION);  //投影矩阵模式
	glLoadIdentity();  //矩阵堆栈清空
	glViewport(0, 0, w, h); //设置视区大小
	//gluOrtho2D(-340, 340, -340, 340);
	//gluOrtho2D(0, 340, 0, 340);
	gluOrtho2D(0, w, 0, h);
	ww = w;
	hh = h;
	glMatrixMode(GL_MODELVIEW);  //模型矩阵模式

}

//鼠标按钮响应事件..
void myMouse(int button, int state, int x, int y)
{
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
	{
		line[k][0] = x;   //线段起点x坐标
		line[k][1] = hh - y;  //线段终点y坐标
	}

	if (button == GLUT_LEFT_BUTTON && state == GLUT_UP)
	{
		line[k][2] = x;   //线段起点x坐标
		line[k][3] = hh - y;   //线段终点y坐标
		k++;
		glutPostRedisplay();
	}
}

//鼠标移动时获得鼠标移动中的坐标-----------------------------------------------------
void myMotion(int x, int y)
{
	//get the line's motion point
	line[k][2] = x;   //动态终点的x坐标
	line[k][3] = hh - y;  //动态终点的y坐标 
	glutPostRedisplay();
}

//画星星
void  draw()
{
	for (int i = 0; i <= k; i++) //********
	{
		/*glBegin(GL_LINES);
		glVertex2f(line[i][0], line[i][1]);
		glVertex2f(line[i][2], line[i][3]);
		glEnd();*/
		//glPushMatrix();
		//glColor3f((118.0 / 255.0), (188.0 / 255.0), (211.0 / 255.0));
		//glTranslatef(line[i][0], line[i][1], 0);//平移函数
		//glBegin(GL_LINES);//AC
		//glVertex2f(0, 30);
		//glVertex2f(30 * cos(54 * PI / 180), -30 * sin(54 * PI / 180));//C
		//glEnd();
		//glBegin(GL_LINES);//AD
		//glVertex2f(0, 30);
		//glVertex2f(-30 * cos(54 * PI / 180), -30 * sin(54 * PI / 180));//D
		//glEnd();
		//glBegin(GL_LINES);//EB
		//glVertex2f(-30 * cos(18 * PI / 180), 30 * sin(18 * PI / 180));//E
		//glVertex2f(30 * cos(18 * PI / 180), 30 * sin(18 * PI / 180));//B
		//glEnd();
		//glBegin(GL_LINES);//DB
		//glVertex2f(-30 * cos(54 * PI / 180), -30 * sin(54 * PI / 180));//D
		//glVertex2f(30 * cos(18 * PI / 180), 30 * sin(18 * PI / 180));//B
		//glEnd();
		//glBegin(GL_LINES);//EC
		//glVertex2f(-30 * cos(18 * PI / 180), 30 * sin(18 * PI / 180));//E
		//glVertex2f(30 * cos(54 * PI / 180), -30 * sin(54 * PI / 180));//C
		//glEnd();
		//glPopMatrix();

		glPushMatrix();
		glColor3f((118.0 / 255.0), (188.0 / 255.0), (211.0 / 255.0));
		glTranslatef(line[i][0], line[i][1], 0);//平移函数
		glBegin(GL_LINES);//AC
		glVertex2f(0, f);
		glVertex2f(f * cos(54 * PI / 180), -f * sin(54 * PI / 180));//C
		glEnd();
		glBegin(GL_LINES);//AD
		glVertex2f(0, f);
		glVertex2f(-f * cos(54 * PI / 180), -f * sin(54 * PI / 180));//D
		glEnd();
		glBegin(GL_LINES);//EB
		glVertex2f(-f * cos(18 * PI / 180), f * sin(18 * PI / 180));//E
		glVertex2f(f * cos(18 * PI / 180), f * sin(18 * PI / 180));//B
		glEnd();
		glBegin(GL_LINES);//DB
		glVertex2f(-f * cos(54 * PI / 180), -f * sin(54 * PI / 180));//D
		glVertex2f(f * cos(18 * PI / 180), f * sin(18 * PI / 180));//B
		glEnd();
		glBegin(GL_LINES);//EC
		glVertex2f(-f * cos(18 * PI / 180), f * sin(18 * PI / 180));//E
		glVertex2f(f * cos(54 * PI / 180), -f * sin(54 * PI / 180));//C
		glEnd();
		glPopMatrix();
	}
}




/************************************************************************/
/* 选择字体函数                                                         */
/************************************************************************/
void selectFont(int size, int charset, const char* face)
{
	HFONT hFont = CreateFontA(size, 0, 0, 0, FW_MEDIUM, 0, 0, 0,
		charset, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,
		DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, face);
	HFONT hOldFont = (HFONT)SelectObject(wglGetCurrentDC(), hFont);
	DeleteObject(hOldFont);
}

/************************************************************************/
/* 生成中文字体函数                                                     */
/************************************************************************/
void drawCNString(const char* str)
{
	int len, i;
	wchar_t* wstring;
	HDC hDC = wglGetCurrentDC();
	GLuint list = glGenLists(1);

	// 计算字符的个数
	// 如果是双字节字符的（比如中文字符），两个字节才算一个字符
	// 否则一个字节算一个字符
	len = 0;
	for (i = 0; str[i] != '\0'; ++i)
	{
		if (IsDBCSLeadByte(str[i]))
			++i;
		++len;
	}

	// 将混合字符转化为宽字符
	wstring = (wchar_t*)malloc((len + 1) * sizeof(wchar_t));
	MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, str, -1, wstring, len);
	wstring[len] = L'\0';

	// 逐个输出字符
	for (i = 0; i<len; ++i)
	{
		wglUseFontBitmapsW(hDC, wstring[i], 1, list);
		glCallList(list);
	}

	// 回收所有临时资源
	free(wstring);
	glDeleteLists(list, 1);
}